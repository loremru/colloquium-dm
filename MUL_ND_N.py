#1306 Кравченко Софья
import unittest
from str_natural_to_arr import Str_D_To_Arr


class MUL_ND_N:
    def __init__(self):
        pass
    
    def MUL_ND_N(self, D, str_digit):

        if str_digit == "0" or D == "0":
            return "0"
        digit = int(str_digit) #цифра
        A = Str_D_To_Arr(D) #длинное число в виде списка
        
        s = 0 #для переноса в старший разряд
        n = len(A) - 1 #номер последней цифры(считая от 0, с конца)
        p = 10 #система счисления
        c = [0]*(n + 1)#для результата
        
        for i in range(n, -1, -1):
            c[i] = (digit*A[i] + s) % p
            s = (digit*A[i] + s) // p
        if s > 0:
            c.insert(0, s) #добавить в начало
            
        return "".join(map(str, c)) #возвращаем в виде строки



class Test_MUL_ND_N(unittest.TestCase):
    
    def setUp(self):
        self.module = MUL_ND_N()
    
    def test_add(self):
        self.assertEqual(self.module.MUL_ND_N("2135830945809328494985940293489032", "5"),
                         "10679154729046642474929701467445160")
        self.assertEqual(self.module.MUL_ND_N("1234567891234567890", "3"),
                         "3703703673703703670")
        self.assertEqual(self.module.MUL_ND_N("57383287575781986764123487", "9"),
                         "516449588182037880877111383")
        self.assertEqual(self.module.MUL_ND_N("57383287575781986764123487", "0"),
                         "0")


if __name__ == "__main__":
    unittest.main()
