# 1306 Абубакиров Марат
import unittest

class ABS_Z_N:
    def __init__(self):
        pass
    def ABS_Z_N(self, Z):
        if Z[0] == "-" or Z[0] == "+": # проверяем, имеет ли число знак
            return Z[1:] # возвращаем число без знака
        else:
            return Z

class TestABS_Z_N(unittest.TestCase):
    def setUp(self):
        self.module = ABS_Z_N()
    def test_add(self):
        self.assertEqual(self.module.ABS_Z_N("582"), ("582"))
        self.assertEqual(self.module.ABS_Z_N("-282"), ("282"))
        self.assertEqual(self.module.ABS_Z_N("0"), ("0"))

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
