# from TEST_MODULE import Calculator
from MUL_Nk_N import MUL_Nk_N
from MUL_ND_N import MUL_ND_N
from COM_NN_D import COM_NN_D
from ADD_1N_N import ADD_1N_N
from TRANS_N_Z import TRANS_N_Z
from ADD_NN_N import ADD_NN_N
from MUL_NN_N import MUL_NN_N
from NZER_N_B import NZER_N_B
from MUL_ZM_Z import MUL_ZM_Z
from TRANS_Z_Q import TRANS_Z_Q
from TRANS_Q_Z import TRANS_Q_Z
from DER_P_P import DER_P_P
from DEG_P_N import DEG_P_N
from MUL_Pxk_P import MUL_Pxk_P
from POZ_Z_D import POZ_Z_D
from ADD_ZZ_Z import ADD_ZZ_Z
from SUB_NN_N import SUB_NN_N
from ABS_Z_N import ABS_Z_N
from TRANS_Z_N import TRANS_Z_N
from DIV_NN_Dk import DIV_NN_Dk
from LED_P_Q import LED_P_Q
from SUB_NDN_N import SUB_NDN_N
from MOD_NN_N import MOD_NN_N
from DIV_NN_N import DIV_NN_N
from DIV_ZZ_Z import DIV_ZZ_Z
from MUL_ZZ_Z import MUL_ZZ_Z
from SUB_ZZ_Z import SUB_ZZ_Z
from LCM_NN_N import LCM_NN_N
from GCF_NN_N import GCF_NN_N
from SUB_QQ_Q import SUB_QQ_Q
from RED_Q_Q import RED_Q_Q
from ADD_PP_P import ADD_PP_P
from SUB_PP_P import SUB_PP_P
from DIV_QQ_Q import DIV_QQ_Q
from FAC_P_Q import FAC_P_Q
from MUL_QQ_Q import MUL_QQ_Q
from MOD_ZZ_Z import MOD_ZZ_Z

from ADD_QQ_Q import ADD_QQ_Q
from MUL_PQ_P import MUL_PQ_P
from MUL_PP_P import MUL_PP_P
# from MOD_PP_P import MOD_PP_P
class Module:
    def __init__(self, func, name, input_rows, description):
        self.func = func
        self.name = name
        self.input_rows = input_rows
        self.description = description
        pass
class Application:
    def __init__(self):
        self.modules = []
        pass

    def register_module(self, func, name, input_rows, description):
        self.modules.append(Module(func, name, input_rows, description))

    def start(self):
        num = 1
        for i in self.modules:
            print("{}. {}".format(num, i.name))
            num += 1
        menu_number = int(input())
        if 0 < menu_number <= len(self.modules):
            current_module = self.modules[menu_number - 1]
            print('Справка по модулю: ' + current_module.description)
            input_arr = []
            for j in range(current_module.input_rows):
                input_arr.append(input())
            print("Ответ: {}".format(current_module.func(*input_arr)))
        else:
            print("Пожалуйста введите корректный номер списка. Запустите приложение заново")
        input()

App = Application()


# Здесь регистрируем модуль в меню
# App.register_module(Calculator().add_for_input, "Сложить два числа", 2, "Введите два числа на разных строках")
App.register_module(TRANS_Z_N().trans_z_n, "Преобразование целого неотрицательного в натуральное", 1, "Введите число")
App.register_module(MUL_Nk_N().MUL_Nk_N, "Умножить натуральное число на 10^k", 2, "Введите число, а затем k")
App.register_module(MUL_ND_N().MUL_ND_N, "Умножить натуральное число на цифру", 2, "Введите число, а затем цифру на разных строках")
App.register_module(COM_NN_D().COM_NN_D, "Сравнить два числа", 2, "Введите два числа на разных строках")
App.register_module(ADD_1N_N().ADD_1N_N, "Прибавить единицу", 1, "Введите одно число")
App.register_module(TRANS_N_Z().TRANS_N_Z, "Преобразовать натуральное в целое", 1, "Введите число")
App.register_module(ADD_NN_N().ADD_NN_N, "Сложить два натуральных числа", 2, "Введите два числа на разных строках")
App.register_module(MUL_NN_N().MUL_NN_N, "Умножить два натуральных числа", 2, "Введите два числа на разных строках")
App.register_module(NZER_N_B().NZER_N_B, "Проверка на 0", 1, "Введите число")
App.register_module(MUL_ZM_Z().MUL_ZM_Z, "умножение на -1", 1, "Введите число")
App.register_module(TRANS_Z_Q().TRANS_Z_Q, "конвертация в дробь", 1, "Введите число")
App.register_module(TRANS_Q_Z().TRANS_Q_Z, "конвертация в целое", 2, "Введите числитель, знаменатель")
App.register_module(DER_P_P().DER_P_P, "Производная многочлена", 1, "Введите коэффициенты при степенях многочлена через запятую")
App.register_module(DEG_P_N().DEG_P_N, "Степень многочлена", 1, "Введите строку коэффициентов через запятую")
App.register_module(MUL_Pxk_P().MUL_Pxk_P, "Умножить многочлен на x^k", 2, "Введите коэффициенты многочлена через запятую, а затем число k на разных строках")
App.register_module(POZ_Z_D().POZ_Z_D, "Определение положительности числа", 1, "Введите число")
App.register_module(ABS_Z_N().ABS_Z_N, "Абсолютное значение", 1, "Введите число")
App.register_module(SUB_NN_N().SUB_NN_N, "Вычитание из первого большего второго (натуральные)", 2, "Введите 2 числа")
App.register_module(ADD_ZZ_Z().ADD_ZZ_Z, "Сложение целых чисел", 2, "Введите 2 числа")
App.register_module(DIV_NN_Dk().DIV_NN_Dk, "Вычисление первой цифры деления большего натурального на меньшее", 2, "Введите числа на двух разных строках")
App.register_module(POZ_Z_D().POZ_Z_D, "Четность числа", 1, "Введите число")
App.register_module(LED_P_Q().LED_P_Q, "Вывод старшего коэффа", 1, "Введите коэффициенты многочлена через запятую")
App.register_module(DIV_NN_N().DIV_NN_N, "Частное от деления большего натурального на меньшее или равное", 2, "Введите два числа на разных строках")
App.register_module(SUB_NDN_N().SUB_NDN_N, "Вычитание из натурального другого натурального, умноженного на цифру для случая с неотрицательным результатом", 3, "Введите два натуральных числа и цифру на трех разных строках")
App.register_module(MOD_NN_N().MOD_NN_N, "Остаток от деления большего натурального на меньшее или равное", 2, "Введите два числа на разных строках")
App.register_module(DIV_NN_N().DIV_NN_N, "Частное от деления большего натурального на меньшее или равное", 2, "Введите два числа на разных строках")
App.register_module(DIV_ZZ_Z().DIV_ZZ_Z, "Частное от деления большего (по модулю) целого числа на меньшее (по модулю) целое число, или равное", 2, "Введите два целых числа на разных строках")
App.register_module(MUL_ZZ_Z().MUL_ZZ_Z, "Умножить два целых числа", 2, "Введите два целых числа на разных строках")
App.register_module(SUB_ZZ_Z().SUB_ZZ_Z, "Вычитание целых чисел", 2, "Введите два числа на разных строках")
App.register_module(LCM_NN_N().LCM_NN_N, "Нахождение НОК двух натуральных чисел", 2, "Введите два числа на разных строках")
App.register_module(GCF_NN_N().GCF_NN_N, "НОД натуральных чисел", 2, "Введите два числа")
App.register_module(SUB_QQ_Q().SUB_QQ_Q, "Вычитание дробей", 2, "Введите два дроби")
App.register_module(RED_Q_Q().RED_Q_Q, "Сокращение дроби", 1, "Введите дробь через /")
App.register_module(ADD_PP_P().ADD_PP_P, "Сложение многочленов", 2, "Введите коэффициенты двух многочленов начиная с наибольшего и заканчивая свободным членом на разных строках") 
App.register_module(SUB_PP_P().SUB_PP_P, "Вычитание многочленов", 2, "Введите коэффициенты двух многочленов на разных строках")
App.register_module(DIV_QQ_Q().DIV_QQ_Q, "Деление двух дробей", 2, "Вводится две дроби на отдельных строках формата +хх/хх")
App.register_module(ADD_QQ_Q().ADD_QQ_Q, "Сложение дробей", 2, "Введите две дроби на разных строках")
App.register_module(MUL_PQ_P().MUL_PQ_P, "Произведение многочлена и рационального числа", 2, "Введите коэффициенты многочлена и рациональный коэффициент на разных строках")
App.register_module(FAC_P_Q().FAC_P_Q, "Вынесение из многочлена НОК знаменателей коэффициентов и НОД числителей", 1, "Введите коэффициенты многочлена через запятую")
App.register_module(MUL_QQ_Q().MUL_QQ_Q, "Умножение дробей", 2, "Введите две дроби на разных строках")
App.register_module(MOD_ZZ_Z().MOD_ZZ_Z, "Остаток от деления целого на целое(делитель отличен от нуля)", 2, "Введите два числа на разных строках")
App.register_module(MUL_PP_P().MUL_PP_P, "Умножение многочленов", 2, "Введите коэффиценты многочленов на разных сторках через запятую")
# App.register_module(MOD_PP_P().MOD_PP_P, "Остаток от деления многочлена на многочлен при делении с остатком", 2, "Введите два числа на разных строках")

App.start()
