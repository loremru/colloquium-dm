# 1306 Тряпша Лиза
import unittest
from str_natural_to_arr import Str_D_To_Arr
from str_int_to_arr import Str_Z_To_Arr
from COM_NN_D import COM_NN_D
from MUL_ZM_Z import MUL_ZM_Z
from SUB_NN_N import SUB_NN_N
from ADD_NN_N import ADD_NN_N
from POZ_Z_D import POZ_Z_D
from ABS_Z_N import ABS_Z_N

class SUB_ZZ_Z:
    def __init__(self):
        pass

    def SUB_ZZ_Z(self, Z1, Z2):
        # разбиваем строку на массив цифр
        A1 = Str_Z_To_Arr(Z1)
        A2 = Str_Z_To_Arr(Z2)
        r1=""
        r2=""
        r1 = Z1
        r2 = Z2
        # берем абсолютные значения
        Z1 = ABS_Z_N().ABS_Z_N(Z1)
        Z2 = ABS_Z_N().ABS_Z_N(Z2)
        # записываем знаковые разряды чисел
        dct = {0: "+", 1: "-"}
        b1 = dct[A1[0]]
        b2 = dct[A2[0]]

        res = ""
        # знаки у чисел равны
        if POZ_Z_D().POZ_Z_D(r1) == POZ_Z_D().POZ_Z_D(r2):
            if COM_NN_D().COM_NN_D(Z1,Z2) == 2:
                res = SUB_NN_N().SUB_NN_N(Z1, Z2)
            else:
                res = SUB_NN_N().SUB_NN_N(Z2, Z1)
            if b1 == "-" and COM_NN_D().COM_NN_D(Z1, Z2) == 2: res = MUL_ZM_Z().MUL_ZM_Z(res)
            if b1 == "+" and COM_NN_D().COM_NN_D(Z1, Z2) == 1: res = MUL_ZM_Z().MUL_ZM_Z(res)
        # знаки у чисел различны
        else:
            res = ADD_NN_N().ADD_NN_N(Z1, Z2)
            # первое число - отрицательное
            if b1 == "-":
                res = MUL_ZM_Z().MUL_ZM_Z(res)
        r = list(res)
        if r[0] != "-" and r[0] != "0":
            res = "+" + res
        return res

class TestADD_1N_N(unittest.TestCase):
    def setUp(self):
        self.module = SUB_ZZ_Z()

    def test_add(self):
        self.assertEqual(self.module.SUB_ZZ_Z("+23", "-23"), "+46")
        self.assertEqual(self.module.SUB_ZZ_Z("-22","-32"), "+10")
        self.assertEqual(self.module.SUB_ZZ_Z("-32", "-22"), "-10")
        self.assertEqual(self.module.SUB_ZZ_Z("-32", "-22"), "-10")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
