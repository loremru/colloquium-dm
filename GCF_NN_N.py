# 1306 Сухарев Игорь
import unittest
from MOD_NN_N import MOD_NN_N
from COM_NN_D import COM_NN_D
from NZER_N_B import NZER_N_B


class GCF_NN_N:
    def __init__(self):
        self.nser = NZER_N_B()
        self.com = COM_NN_D()
        self.mod = MOD_NN_N()

    def GCF_NN_N(self, D1, D2):
        # проверяем на наличие 0
        if (self.nser.NZER_N_B(D1) is False) or (self.nser.NZER_N_B(D2) is False):
            if (self.nser.NZER_N_B(D1) is False) and (self.nser.NZER_N_B(D2) is False):
                return '0' # если оба 0 выводим 0
            elif (self.nser.NZER_N_B(D1) is False) or (self.nser.NZER_N_B(D2) is not False):
                # если одно из чисел 0, выводим второе
                return D2
            else:
                return D1
        else:
            # определяем, какое из чисел больше
            if self.com.COM_NN_D(D1, D2) == 2:
                n1 = D1
                n2 = D2
                k = 1
                while k != '0':
                    # выполняем алгоритм Евклида
                    k = self.mod.MOD_NN_N(n1, n2)
                    n1, n2 = n2, k
                return n1
            elif self.com.COM_NN_D(D1, D2) == 1:
                n1 = D2
                n2 = D1
                k = 1
                while k != '0':
                    # выполняем алгоритм Евклида
                    k = self.mod.MOD_NN_N(n1, n2)
                    n1, n2 = n2, k
                return n1
            else:
                return D1


class TestGCF_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = GCF_NN_N()

    def test_add(self):
        self.assertEqual(self.module.GCF_NN_N('0', '0'), '0')
        self.assertEqual(self.module.GCF_NN_N('15', '5'), '5')
        self.assertEqual(self.module.GCF_NN_N('555', '7'), '1')
        self.assertEqual(self.module.GCF_NN_N('24', '16'), '8')


if __name__ == "__main__":
    unittest.main()
