# 1306 Pesterev Victor
import unittest
from MUL_QQ_Q import MUL_QQ_Q
from str_rational_to_arr import Str_Q_To_Arr


class DIV_QQ_Q:
    def __init__(self):
        pass

    def DIV_QQ_Q(self, Q01, Q02):
        zn = '+'
        if(len(Q02)>1):    #получаем знак второй дроби, если она не является натуральным числом
            zn = Q02[0]
            Q02 = Q02[1:]

        if(Q02=='0'):
            return "Div 0 error!"

        B = Str_Q_To_Arr(Q02)  #разделяем вторую дробь на числитель и знаменатель

        Q = zn + str(B[1]) + "/" + str(B[0])  # собираем дробь в строку

        return MUL_QQ_Q().MUL_QQ_Q(Q01, Q)  # умножаем первую дробь на перевернутую вторую


class TestDIV_QQ_Q(unittest.TestCase):
    def setUp(self):
        self.module = DIV_QQ_Q()

    def test_add(self):
        self.assertEqual(self.module.DIV_QQ_Q("+2/7", "+5/8"), "+16/35")
        self.assertEqual(self.module.DIV_QQ_Q("+4/7", "+9/8"), "+32/63")
        self.assertEqual(self.module.DIV_QQ_Q("+3/2", "2"), "+3/4")
        self.assertEqual(self.module.DIV_QQ_Q("+79/156", "+79/156"), "+1")
        self.assertEqual(self.module.DIV_QQ_Q("+79/156", "-156/79"), "-6241/24336")
        self.assertEqual(self.module.DIV_QQ_Q("-79/156", "-79/156"), "+1")
        self.assertEqual(self.module.DIV_QQ_Q("-79/156", "0"), "Div 0 error!")
        self.assertEqual(self.module.DIV_QQ_Q("0", "10/15"), "0")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()