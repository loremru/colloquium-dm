# 1306 Kolosov Kostyan
import unittest
from DEG_P_N import DEG_P_N
from POZ_Z_D import POZ_Z_D
from ABS_Z_N import ABS_Z_N
from TRANS_Z_N import TRANS_Z_N
from GCF_NN_N import GCF_NN_N
from LCM_NN_N import LCM_NN_N
from TRANS_N_Z import TRANS_N_Z
from str_poly_to_arr import str_p_to_arr
from str_rational_to_arr import Str_Q_To_Arr
from LED_P_Q import LED_P_Q

class FAC_P_Q:
    def __init__(self):
        pass



    def FAC_P_Q(self, C):
        arr = str_p_to_arr(C)
        m = DEG_P_N().DEG_P_N(C)#степень многочлена
        nok = []
        nod = []
        i = 0
        a=[]
        b=[]
        for i in range(len(arr)):#разбиваем каждый коэф. на числитель и знаменатель и добавляем в соответсвтующие массивы
            A=LED_P_Q().LED_P_Q(arr[i])
            B=Str_Q_To_Arr(A)
            a.append(B[0])
            b.append(B[1])



        for i in range(m):
            if POZ_Z_D().POZ_Z_D(a[i]) == 1:#если коэф. отрицательный - меняем на абсолют. величину
                a[i] = ABS_Z_N().ABS_Z_N(a[i])
            elif POZ_Z_D().POZ_Z_D(b[i]) == 1:#если коэф. отрицательный - меняем на абсолют. величину
                b[i] = ABS_Z_N().ABS_Z_N(b[i])
            else:
                a[i] = TRANS_Z_N().trans_z_n(a[i])#иначе преобразовываем в натуральное
                b[i] = TRANS_Z_N().trans_z_n(b[i])  # иначе преобразовываем в натуральное

        if m == 0:
            nod = a[i]
            nok = b[i]
        else:
            for i in range(0, m + 1):
                if i == 0:
                    nod = GCF_NN_N().GCF_NN_N(a[i], a[i + 1])# Чтобы найти НОД чисел, сначала нужно найти НОД двух первых чисел
                    nok = LCM_NN_N().LCM_NN_N(b[i], b[i + 1])# Чтобы найти НОК чисел, сначала нужно найти НОК двух первых чисел
                else:
                    if i < m:
                        nod = GCF_NN_N().GCF_NN_N(nod, a[i + 1])#потом находим НОД вот таким образом: НОД(НОД двух предыдущих чисел, следующее число)
                        nok = LCM_NN_N().LCM_NN_N(nok, b[i + 1])#потом находим НОК вот таким образом: НОК(НОК двух предыдущих чисел, следующее число)
        return nok,TRANS_N_Z().TRANS_N_Z(nod)


class TestFAC_P_Q(unittest.TestCase):
    def setUp(self):
        self.module = FAC_P_Q()

    def test_add(self):
        self.assertEqual(self.module.FAC_P_Q('-5/1,2/1,45/1,6/1'), ("1", "+1"))
        self.assertEqual(self.module.FAC_P_Q("15/2,30/1,45/8"), ("8", "+15"))
        self.assertEqual(self.module.FAC_P_Q("2/5,24/7,8/3"), ("105", "+2"))
        self.assertEqual(self.module.FAC_P_Q("1/5,4/9"), ("45", "+1"))


# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()