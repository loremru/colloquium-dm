# 1306 Вилков Дмитрий
import unittest

class TRANS_Z_Q:
    def __init__(self):
        pass
    def TRANS_Z_Q(self, X):
        return X+"/1" #возвращает само число и 1 как его знаменатель

class TestTRANS_Z_Q(unittest.TestCase):
    def setUp(self):
        self.module = TRANS_Z_Q()

    def test_add(self):
        self.assertEqual(self.module.TRANS_Z_Q("-582"), ("-582/1"))
        self.assertEqual(self.module.TRANS_Z_Q("6745026"), ("6745026/1"))

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
