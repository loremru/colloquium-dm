# 1306 Исаков Илья
import unittest
from str_natural_to_arr import Str_D_To_Arr

class Calculator:
    def __init__(self):
        pass
    def add_for_input(self, x1: str, x2: str): # Входные параметры тип str
        return self.add(int(x1), int(x2)) # Преобразуем и отправляем на вычисления
    def add(self, x1, x2):
        return x1 + x2

class TestCalculator(unittest.TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_add(self):
        self.assertEqual(self.calculator.add(4, 7), 11)

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
