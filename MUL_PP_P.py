# 1306 Крюкова Мария
import unittest
from MUL_PQ_P import MUL_PQ_P
from MUL_Pxk_P import MUL_Pxk_P
from ADD_PP_P import ADD_PP_P
from DEG_P_N import DEG_P_N
from arr_poly_to_str import arr_p_to_str
from str_poly_to_arr import str_p_to_arr


class MUL_PP_P:
    def __init__(self):
        pass

    def MUL_PP_P(self, C1, C2):
        C1 = str_p_to_arr(C1)
        C2 = str_p_to_arr(C2)
        if len(C1) >= len(C2):  # проверяем, в каком из многочленов больше коэффициентов
            answer = ("0, " * (DEG_P_N().DEG_P_N(arr_p_to_str(C1)) + 1)).strip()[
                     :-1]  # создаем нулевой многочлен, к которому будут прибавляться другие
            answer = MUL_Pxk_P().MUL_Pxk_P(answer, DEG_P_N().DEG_P_N(
                arr_p_to_str(C2)))  # увеличиваем его степень на степень второго многочлена
            for i in range(len(C2)):
                arr = MUL_PQ_P().MUL_PQ_P(arr_p_to_str(C1), C2[
                    i])  # умножаем каждый коэффициент первого многочлена на коэффициент второго
                arr = MUL_Pxk_P().MUL_Pxk_P(arr, DEG_P_N().DEG_P_N(arr_p_to_str(C2)) - i)  # увеличиваем степень
                answer = ADD_PP_P().ADD_PP_P(answer, arr)  # прибавляем к финальному многочлену
        else:
            answer = MUL_PP_P().MUL_PP_P(C2, C1)
        return answer


class TestMUL_PP_P(unittest.TestCase):
    def setUp(self):
        self.module = MUL_PP_P()

    def test_add(self):
        self.assertEqual("+3, 0, +19/2, 0, -11/2, -18, +24, -45/2, +137/2, +49, -93, -143, +170, +87, -105",
                         self.module.MUL_PP_P("+1, 0, +3/2, 0, -3, -3, +8, +2, -5",
                                              "+3, 0, +5, 0, -4, -9, +21"))


# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()