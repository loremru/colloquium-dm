#1306 Кравченко Софья
import unittest
from str_poly_to_arr import str_p_to_arr
from arr_poly_to_str import arr_p_to_str
from MUL_QQ_Q import MUL_QQ_Q
from DEG_P_N import DEG_P_N
from MUL_ZZ_Z import MUL_ZZ_Z
from TRANS_Z_Q import TRANS_Z_Q

class DER_P_P:
    def __init__(self):
        self.mul_q = MUL_QQ_Q()
        self.mul_z = MUL_ZZ_Z()
        self.trns = TRANS_Z_Q()

    def DER_P_P(self, C):
        n = DEG_P_N().DEG_P_N(C)  #степень многочлена
        C = str_p_to_arr(C)
        C_res = [0] * n  #массив коэффициентов для производной
        a = n #для степени текущего одночлена(от большей к меньшей)
        for i in range(n):
            if not "/" in C[i]:  #если  коэффициент не дробный, то умножение целых, иначе - дробей
                #умножаем степень многочлена на коэффициент при этой степени
                C_res[i] = self.mul_z.MUL_ZZ_Z(C[i], str(a))
            else:
                C_res[i] = self.mul_q.MUL_QQ_Q(C[i],
                            self.trns.TRANS_Z_Q(str(a))) #переведем степень в дробное со знаменателем 1
            a = a - 1;
        return arr_p_to_str(C_res) #вернем в виде строки


class Test_DER_P_P(unittest.TestCase):

    def setUp(self):
        self.module = DER_P_P()

    def test_add(self):
        self.assertEqual(self.module.DER_P_P("+1, +1, 1, -1, 0, 1"), "+5, +4, +3, -2, 0")
        self.assertEqual(self.module.DER_P_P("10, 0, 0, 0"), "+30, 0, 0")
        self.assertEqual(self.module.DER_P_P("2, 10"), "+2")
        self.assertEqual(self.module.DER_P_P("6, -4, 20, 15, 1000"), "+24, -12, +40, +15")
        self.assertEqual(self.module.DER_P_P("1, 0, 3, 0, 5, 0, 7, 0, 9"), "+8, 0, +18, 0, +20, 0, +14, 0")
        self.assertEqual(self.module.DER_P_P("-1, 5, 0, 0, 0, 0, 0, 0, 6, 0, 16"), "-10, +45, 0, 0, 0, 0, 0, 0, +12, 0")
        #self.assertEqual(self.module.DER_P_P("1/3, -1/2, 10, 0"), "3, -1, +10")


if __name__ == "__main__":
    unittest.main()