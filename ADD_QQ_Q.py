# 1306 Тряпша Лиза
import unittest
from LCM_NN_N import LCM_NN_N
from MUL_ZZ_Z import MUL_ZZ_Z
from ADD_ZZ_Z import ADD_ZZ_Z
from DIV_ZZ_Z import DIV_ZZ_Z
from str_rational_to_arr import Str_Q_To_Arr
from RED_Q_Q import RED_Q_Q

class ADD_QQ_Q:
    def __init__(self):
        pass

    def ADD_QQ_Q(self, Q1, Q2):
        qq1 = Str_Q_To_Arr(Q1) #разбиваем дроби на массивы состоящие из числителя и знаменателя
        qq2 = Str_Q_To_Arr(Q2)
        A1 = qq1[0] # присваеваем числители и знаменатели соответственно
        B1 = qq1[1]
        A2 = qq2[0]
        B2 = qq2[1]
        B = LCM_NN_N().LCM_NN_N(B1,B2) # находим нок знаменателей
        # складываем числители, умножив каждый на частное нока и знаменателя другой дроби
        A = ADD_ZZ_Z().ADD_ZZ_Z(MUL_ZZ_Z().MUL_ZZ_Z(A1,DIV_ZZ_Z().DIV_ZZ_Z(B,B1)),MUL_ZZ_Z().MUL_ZZ_Z(A2,DIV_ZZ_Z().DIV_ZZ_Z(B,B2)))
        F = A + "/" + B
        Q = RED_Q_Q().RED_Q_Q(F) # сокращаем дробь


        return Q

class TestADD_QQ_Q(unittest.TestCase):
    def setUp(self):
        self.module = ADD_QQ_Q()

    def test_add(self):
        self.assertEqual(self.module.ADD_QQ_Q("+1/3", "+1/3"), "+2/3")
        self.assertEqual(self.module.ADD_QQ_Q("-1/2", "-1/3"), "-5/6")
        self.assertEqual(self.module.ADD_QQ_Q("+1/2", "-1/3"), "+1/6")





# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
