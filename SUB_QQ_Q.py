#Бельский Игорь 1306
import unittest
from str_int_to_arr import Str_Z_To_Arr
from LCM_NN_N import LCM_NN_N as LCM
from MUL_ZZ_Z import MUL_ZZ_Z as MUL
from SUB_ZZ_Z import SUB_ZZ_Z as SUB
from DIV_ZZ_Z import DIV_ZZ_Z as DIV
from RED_Q_Q import RED_Q_Q as RED
from str_rational_to_arr import Str_Q_To_Arr

class SUB_QQ_Q:
    def __init__(self):
        pass
    def SUB_QQ_Q(self, Q1, Q2):
        Q1 = Str_Q_To_Arr(Q1)
        Q2 = Str_Q_To_Arr(Q2)
        #выделяем числители и знаменатели дробей
        p1 = Q1[0]
        q1 = Q1[1]
        p2 = Q2[0]
        q2 = Q2[1]
        #находим общий знаменатель
        q = LCM().LCM_NN_N(q1, q2)
        #находим дополнительные множители
        a = DIV().DIV_ZZ_Z(q, q1)
        b = DIV().DIV_ZZ_Z(q, q2)
        #формируем числитель результирующей дроби
        p = SUB().SUB_ZZ_Z(MUL().MUL_ZZ_Z(p1, a), MUL().MUL_ZZ_Z(p2, b))
        #формируем и сокращаем дробь
        res = p + "/" + q
        if p == "0":
            res = "0"
        else:
             res = RED().RED_Q_Q(res)
        return res
        
class TestCOM_NN_D(unittest.TestCase):
    def setUp(self):
        self.module = SUB_QQ_Q()

    def test_add(self):
        self.assertEqual(self.module.SUB_QQ_Q("-10/4", "+4/5"), "-33/10")
        self.assertEqual(self.module.SUB_QQ_Q("+1/3", "1/3"), "0")
        self.assertEqual(self.module.SUB_QQ_Q("2/5", "-1/5"), "+3/5")
        self.assertEqual(self.module.SUB_QQ_Q("1/3", "-1/2"), "+5/6")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()

