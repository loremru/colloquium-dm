#Бельский Игорь 1306
import unittest
from str_int_to_arr import Str_Z_To_Arr
from COM_NN_D import COM_NN_D
from MUL_ZM_Z import MUL_ZM_Z
from SUB_NN_N import SUB_NN_N
from ADD_NN_N import ADD_NN_N
from POZ_Z_D import POZ_Z_D
from ABS_Z_N import ABS_Z_N

class ADD_ZZ_Z:
    def __init__(self):
        pass
    def ADD_ZZ_Z(self, Z1, Z2):
        #разбиваем строку на массив цифр
        A1 = Str_Z_To_Arr(Z1)
        A2 = Str_Z_To_Arr(Z2)
        #берем абсолютные значения
        Z1 = ABS_Z_N().ABS_Z_N(Z1)
        Z2 = ABS_Z_N().ABS_Z_N(Z2)
        #сравниваем числа, в Z1 будет находиться большее по модулю
        if COM_NN_D().COM_NN_D(Z1, Z2) == 1:
            Z1, Z2 = Z2, Z1
            A1, A2 = A2, A1
        #записываем знаки чисел
        dct = {0: "+", 1: "-"}
        b1 = dct[A1[0]]
        b2 = dct[A2[0]]
        
        res = ""
        #значения равны, но знаки противоположны (или 0)
        if Z1 == Z2 and b1 != b2 or Z1 == "0":
            res = "0"
        #знаки у чисел одинаковы
        elif POZ_Z_D().POZ_Z_D(b1 + Z1) == POZ_Z_D().POZ_Z_D(b2 + Z2):
           res = ADD_NN_N().ADD_NN_N(Z1, Z2)
           if b1 == "-":
               res = MUL_ZM_Z().MUL_ZM_Z(res)
           else:
               res = b1 + res
        #знаки у чисел различны
        else:
            res = SUB_NN_N().SUB_NN_N(Z1, Z2)
            #большее число - отрицательное
            if POZ_Z_D().POZ_Z_D(b1 + Z1) == 1 and Z2 != "0":
                res = MUL_ZM_Z().MUL_ZM_Z(res)
            else:
                res = b1 + res
        return res


class TestCOM_NN_D(unittest.TestCase):
    def setUp(self):
        self.module = ADD_ZZ_Z()

    def test_add(self):
        self.assertEqual(self.module.ADD_ZZ_Z("-10", "-4"), "-14")
        self.assertEqual(self.module.ADD_ZZ_Z("-40", "-15"), "-55")
        self.assertEqual(self.module.ADD_ZZ_Z("+10", "+4"), "+14")
        self.assertEqual(self.module.ADD_ZZ_Z("+10", "+10"), "+20")
        self.assertEqual(self.module.ADD_ZZ_Z("+4", "+10"), "+14")
        self.assertEqual(self.module.ADD_ZZ_Z("0", "+4"), "+4")
        self.assertEqual(self.module.ADD_ZZ_Z("-4", "0"), "-4")
        self.assertEqual(self.module.ADD_ZZ_Z("0", "0"), "0")
        self.assertEqual(self.module.ADD_ZZ_Z("+14", "-14"), "0")
        self.assertEqual(self.module.ADD_ZZ_Z("-14", "+14"), "0")
        self.assertEqual(self.module.ADD_ZZ_Z("+14", "-12"), "+2")
        self.assertEqual(self.module.ADD_ZZ_Z("-12", "+14"), "+2")
        self.assertEqual(self.module.ADD_ZZ_Z("-19", "+5"), "-14")
        self.assertEqual(self.module.ADD_ZZ_Z("+5", "-29"), "-24")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()

