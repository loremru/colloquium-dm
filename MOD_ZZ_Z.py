# 1306 Крюкова Мария

from DIV_ZZ_Z import DIV_ZZ_Z

from MUL_ZZ_Z import MUL_ZZ_Z

from SUB_ZZ_Z import SUB_ZZ_Z

import unittest


class MOD_ZZ_Z:
    def __init__(self):
        pass

    def MOD_ZZ_Z(self, D1, D2):
        res = SUB_ZZ_Z().SUB_ZZ_Z(D1, MUL_ZZ_Z().MUL_ZZ_Z(D2, DIV_ZZ_Z().DIV_ZZ_Z(D1, D2)))
        return res


class TestMOD_ZZ_Z(unittest.TestCase):
    def setUp(self):
        self.module = MOD_ZZ_Z()

    def test_add(self):
        self.assertEqual(self.module.MOD_ZZ_Z("123", "120"), "+3")
        self.assertEqual(self.module.MOD_ZZ_Z("55", "-10"), "-5")
        self.assertEqual(self.module.MOD_ZZ_Z("-853", "2"), "+1")
        self.assertEqual(self.module.MOD_ZZ_Z("-269", "-2"), "+1")


# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    # print(MOD_ZZ_Z().MOD_ZZ_Z("-269", "-2"))
    unittest.main()