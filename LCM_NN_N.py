#Вилков Дмитрий 1306
import unittest
from GCF_NN_N import GCF_NN_N
from MUL_NN_N import MUL_NN_N
from DIV_NN_N import DIV_NN_N

class LCM_NN_N:
    def __init__(self):
        pass
    def LCM_NN_N(self, N1, N2):
        tmp=MUL_NN_N().MUL_NN_N(N1,N2) #перемножение исходных чисел 
        tmp2=GCF_NN_N().GCF_NN_N(N1,N2) #нахождение их НОД
        return DIV_NN_N().DIV_NN_N(tmp,tmp2) #возврат резульата деления произведения на НОД

class TestLCM_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = LCM_NN_N()

    def test_add(self):
        self.assertEqual(self.module.LCM_NN_N("36", "48"), "144")
        self.assertEqual(self.module.LCM_NN_N("24", "12"), "24")
        self.assertEqual(self.module.LCM_NN_N("54", "86"), "2322")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
