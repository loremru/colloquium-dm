# 1306 Isaev Ruslan
import unittest
from arr_poly_to_str import arr_p_to_str
class LED_P_Q:
    def __init__(self):
        pass
    
    def LED_P_Q(self, C):
        arr = arr_p_to_str(C)
        C=C.split(",")
        g = C[0] # вывод старшего (1) коэффицента 
        return g
    
class TestLED_P_Q(unittest.TestCase):
    def setUp(self):
        self.module = LED_P_Q()

    def test_add(self):
        self.assertEqual(self.module.LED_P_Q("40, 0, 0 ,2, 0, 0"), "40")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
