from DIV_NN_Dk import DIV_NN_Dk

from COM_NN_D import COM_NN_D

from ADD_NN_N import ADD_NN_N

from SUB_NDN_N import SUB_NDN_N

import unittest

class DIV_NN_N:
    def __init__(self):
        pass

    def DIV_NN_N(self, D1,D2):
        if COM_NN_D().COM_NN_D(D1, D2) == 1:  # в случае, если D2 больше D1, делаем D1 бОльшим числом
            D1, D2 = D2, D1
        if COM_NN_D().COM_NN_D(D1, D2) == 0:  # в случае, если D1 = D2, выводим частное = 1.
            return "1"
        D3="0"
        while COM_NN_D().COM_NN_D(D1, D2) == 2 or COM_NN_D().COM_NN_D(D1, D2) == 0:
            tmp=DIV_NN_Dk().DIV_NN_Dk(D1,D2)
            D1=SUB_NDN_N().SUB_NDN_N(D1,D2,tmp)
            D3=ADD_NN_N().ADD_NN_N(D3,tmp)
        return D3

class TestDIV_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = DIV_NN_N()

    def test_add(self):
        self.assertEqual(self.module.DIV_NN_N("1999","999"), "2")
        self.assertEqual(self.module.DIV_NN_N("12345", "5"), "2469")
        self.assertEqual(self.module.DIV_NN_N("12350", "5"), "2470")
        self.assertEqual(self.module.DIV_NN_N("100100", "100"), "1001")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
