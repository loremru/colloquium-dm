# 1306 Павел Смуров
import unittest
from MUL_Nk_N import MUL_Nk_N
from COM_NN_D import COM_NN_D
from MUL_ND_N import MUL_ND_N

class DIV_NN_Dk:
    def __init__(self):
        pass

    def DIV_NN_Dk(self, a, b):  # принимаем два длинных числа
        i = 1
        if COM_NN_D().COM_NN_D(a, b) == 1:  # делаем a бОльшим числом
            a, b = b, a
        n1 = len(a)
        n2 = len(b)
        k = n1 - n2  # счётчик степени
        num1 = a[:n2]  # берём первые n2 цифр большего числа
        num2 = b  # переименовываем для удобства меньшее число
        if COM_NN_D().COM_NN_D(num1, num2) == 1:  # если num1 меньше num2 то переходим на один разряд
            num1 = a[:n2 + 1]
            k -= 1
        while (COM_NN_D().COM_NN_D(num1, MUL_ND_N().MUL_ND_N(num2, str(i))) != 1):  # ищем первое число, которое при умножении на num2 будет больше num1
            i += 1
        i -= 1  # берём на одну единицу меньше потому что (i-1)*num2 < num1 < i*num2
        ans = MUL_Nk_N().MUL_Nk_N(str(i), k)
        return ans


class TestDIV_NN_Dk(unittest.TestCase):
    def setUp(self):
        self.module = DIV_NN_Dk()

    def test_add(self):
        self.assertEqual(self.module.DIV_NN_Dk("1234567", "300"), "4000")
        self.assertEqual(self.module.DIV_NN_Dk("24", "356"), "10")
        self.assertEqual(self.module.DIV_NN_Dk("88005553535", "7"), "10000000000")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
