# 1306 Пестерев Виктор

import unittest
from ADD_NN_N import ADD_NN_N
from MUL_ND_N import MUL_ND_N
from MUL_Nk_N import MUL_Nk_N
from COM_NN_D import COM_NN_D

class MUL_NN_N:
    def __init__(self):
        pass

    def MUL_NN_N(self, D1, D2):
        #большее число делаем первым
        if COM_NN_D().COM_NN_D(D1, D2)==1:
            D1, D2 = D2, D1

        D3 = "0"
        k = 0
        #умножаем большее число на каждую ненулевую цифру меньшего
        #с учетом разряда и складываем
        for i in D2[::-1]:
            if(i not in '0'):
                D3 = ADD_NN_N().ADD_NN_N(D3, MUL_Nk_N().MUL_Nk_N(MUL_ND_N().MUL_ND_N(D1, i), k))
            k += 1
        return D3


class TestMUL_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = MUL_NN_N()

    def test_add(self):
        self.assertEqual(self.module.MUL_NN_N("1","1"), "1")
        self.assertEqual(self.module.MUL_NN_N("4", "3"), "12")
        self.assertEqual(self.module.MUL_NN_N("999", "999"), "998001")
        self.assertEqual(self.module.MUL_NN_N("0", "999"), "0")
        self.assertEqual(self.module.MUL_NN_N("123", "456"), "56088")
        self.assertEqual(self.module.MUL_NN_N("20000", "200"), "4000000")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()