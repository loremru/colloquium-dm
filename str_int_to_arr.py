def Str_Z_To_Arr(Z):
    A=[]
    if Z[0]=="-":
        A.append(1)
    else:
        A.append(0)
    for i in range(len(Z)):
        if Z[i]!="-" and Z[i]!="+":
            A.append(int(Z[i]))
    return A
