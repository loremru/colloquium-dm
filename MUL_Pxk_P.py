# 1306 Павел Смуров
import unittest
from str_poly_to_arr import str_p_to_arr
from arr_poly_to_str import arr_p_to_str

class MUL_Pxk_P:
    def __init__(self):
        pass

    def MUL_Pxk_P(self, C, k):
        arr = str_p_to_arr(C)
        for i in range(int(k)):
            arr.append(0)  # добавляем k нулей в конец списка
        string = arr_p_to_str(arr)
        return string

class TestMUL_Pxk_P(unittest.TestCase):
    def setUp(self):
        self.module = MUL_Pxk_P()

    def test_add(self):
        self.assertEqual(self.module.MUL_Pxk_P("40, 0, 0 ,2, 0, 0", 1), "40, 0, 0, 2, 0, 0, 0")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
