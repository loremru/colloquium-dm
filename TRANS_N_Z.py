# 1306 Сухарев Игорь
import unittest
from str_natural_to_arr import Str_D_To_Arr


class TRANS_N_Z:
    def __init__(self):
        pass

    def TRANS_N_Z(self, A):
        A = Str_D_To_Arr(A)  # перевод числа из строки в список
        A.insert(0, '+')  # добавляем + в начало списка под знак числа
        A = "".join(map(str, A))  # перевод числа обратно в строку
        return A


class TestTRANS_N_Z(unittest.TestCase):
    def setUp(self):
        self.module = TRANS_N_Z()

    def test_add(self):
        self.assertEqual(self.module.TRANS_N_Z('12345'), '+12345')


if __name__ == "__main__":
    unittest.main()
