# 1306 Крюкова Мария
import unittest

class POZ_Z_D:
    def __init__(self):
        pass
    def POZ_Z_D (self, D):
        res = 0
        if D == '0':  # если число равно нулю, то возвращаем ноль
            res = 0
        elif D[0] == '-': # если число отрицательное, то возвращаем единицу
            res = 1
        else: # если число положительное, то возвращаем двойку
            res = 2
        return res

class TestPOZ_Z_D(unittest.TestCase):
    def setUp(self):
        self.module = POZ_Z_D()

    def test_add(self):
        self.assertEqual(self.module.POZ_Z_D('-11'), 1)

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
