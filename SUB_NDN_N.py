# 1306 Kolosov Kostyan
import unittest
from COM_NN_D import COM_NN_D
from MUL_ND_N import MUL_ND_N
from SUB_NN_N import SUB_NN_N

class SUB_NDN_N:
    def __init__(self):
        pass

    def SUB_NDN_N(self, A, B, n):
        B = MUL_ND_N().MUL_ND_N(B, n) # находим произведение B и n
        if COM_NN_D().COM_NN_D(A, B) == 2 or COM_NN_D().COM_NN_D(A, B) == 0: #Если результат неотрицательный, то находим разность двух натуральных чисел
            A = SUB_NN_N().SUB_NN_N(A, B)
            return A
        else: # иначе возвращаем -1
            return "error in SUB_NDN_N"

class TestSUB_NDN_N(unittest.TestCase):
    def setUp(self):
        self.module = SUB_NDN_N()

    def test_add(self):
        self.assertEqual(self.module.SUB_NDN_N("50", "4", "2"), "42")
        self.assertEqual(self.module.SUB_NDN_N("20", "5", "2"), "10")
        self.assertEqual(self.module.SUB_NDN_N("54213", "85", "36"), "51153")
        self.assertEqual(self.module.SUB_NDN_N("4621345", "150", "3459"), "4102495")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()