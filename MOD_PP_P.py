#1306 Васильев Сергей

from DIV_PP_P import DIV_PP_P
 
from MUL_PP_P import MUL_PP_P
 
from SUB_PP_P import SUB_PP_P
 
 
import unittest
 
 
class MOD_PP_P:
    def __init__(self):
        pass

    #нахождение остатка
    def MOD_PP_P(self, D1, D2):
        res = SUB_PP_P().SUB_PP_P(D1, MUL_PP_P().MUL_PP_P(D2, DIV_PP_P().DIV_PP_P(D1, D2)))
        return res
 
 
class TestMOD_PP_P(unittest.TestCase):
    def setUp(self):
        self.module = MOD_PP_P()
 
    def test_add(self):
        self.assertEqual(self.module.MOD_PP_P("+3,-4,+5,-7", "+1,+5,-2"), "+106,-45")
 
 
# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
