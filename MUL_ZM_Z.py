# 1306 Вилков Дмитрий
import unittest
from str_int_to_arr import Str_Z_To_Arr

class MUL_ZM_Z:
    def __init__(self):
        pass
    def MUL_ZM_Z(self, X):
        if X == "0": return "0" #возвращает 0 без знака
        res=[]
        X=Str_Z_To_Arr(X) #конвертация строки в список
        if X[0]==0: #подставление знака вместо знакового бита
            res.append("-")
        elif X[0]==1:
            res.append("+")
        for i in range(1,len(X)): # запись числа после знакового бита
            res.append(str(X[i]))
        return "".join(map(str, res)) # конвертация обратно в строку и вывод

class TestMUL_ZM_Z(unittest.TestCase):
    def setUp(self):
        self.module = MUL_ZM_Z()

    def test_add(self):
        self.assertEqual(self.module.MUL_ZM_Z("+5"), "-5")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
