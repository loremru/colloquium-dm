# 1306 Пестерев Виктор

import unittest
from str_natural_to_arr import Str_D_To_Arr
from ADD_1N_N import ADD_1N_N
from COM_NN_D import COM_NN_D

class ADD_NN_N:
    def __init__(self):
        pass

    def ADD_NN_N(self, D1, D2):
        #ставим на первое место большее число
        if (COM_NN_D().COM_NN_D(D1, D2) == 1):
            D1, D2 = D2, D1
        D1 = Str_D_To_Arr(D1)[::-1]
        D2 = Str_D_To_Arr(D2)[::-1]
        D3 = []
        tmp = 0
        i = 0
        #складываем поразрядно пока в одном из чисел не кончатся разряды
        for i in range(len(D2)):
            #если сумма разрядов превышает 10, то запоминаем 1 для след разряда
            if ((D1[i] + D2[i] + tmp) >= 10):
                D3.append(D1[i] + D2[i] + tmp - 10)
                tmp = 1
            else:
                D3.append(D1[i] + D2[i] + tmp)
                tmp = 0
        #добавляем оставшиеся разряды
        if i < len(D1) - 1:
            #увеличиваем на 1, если это требуется
            if tmp:
                #от и+1 элемента переводим строку, разворачиваем, прибавляем 1, разворачиваем обратно
                D3 += [int(x) for x in ADD_1N_N().ADD_1N_N("".join(str(x) for x in D1[i + 1:][::-1]))[::-1]]
            else:
                D3 += D1[i + 1:len(D1)]

        #создаем новый разряд, если в результате сложения их число увеличилось
        elif tmp:
            D3.append(tmp)

        return "".join(str(x) for x in D3[::-1])

class TestADD_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = ADD_NN_N()

    def test_add(self):
        self.assertEqual(self.module.ADD_NN_N("1999","999"), "2998")
        self.assertEqual(self.module.ADD_NN_N("4", "3"), "7")
        self.assertEqual(self.module.ADD_NN_N("999", "2"), "1001")
        self.assertEqual(self.module.ADD_NN_N("2", "999"), "1001")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()