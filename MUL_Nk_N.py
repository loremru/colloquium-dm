# 1306 Павел Смуров
import unittest
from str_natural_to_arr import Str_D_To_Arr

class MUL_Nk_N:
    def __init__(self):
        pass

    def MUL_Nk_N(self, A, k):
        A = Str_D_To_Arr(A)  # переводим число, представленное в виде строки, в список
        k = int(k)
        if A != ["0"] and A != [0]:  # умножение на нуль даёт нуль
            for i in range(k):
                A.append(0)  # добавляем k нулей в конец списка
        A = "".join(map(str, A))  # переводим число обратно в строку
        return A

class TestMUL_Nk_N(unittest.TestCase):
    def setUp(self):
        self.module = MUL_Nk_N()

    def test_add(self):
        self.assertEqual(self.module.MUL_Nk_N("156561", "2"), "15656100")
        self.assertEqual(self.module.MUL_Nk_N("0", "10"), "0")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
