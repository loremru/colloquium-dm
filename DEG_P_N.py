# 1306 Сухарев Игорь
import unittest
from str_poly_to_arr import str_p_to_arr

class DEG_P_N:
    def __init__(self):
        pass
    def DEG_P_N(self, C):
        C = str_p_to_arr(C) # перевод строки коэффициентов в список
        m = len(C) - 1 # получение степени многочлена
        return m

class TestTDEG_P_N(unittest.TestCase):
    def setUp(self):
        self.module = DEG_P_N()

    def test_add(self):
        self.assertEqual(self.module.DEG_P_N('15, 0, 2, 3'), 3)

if __name__ == "__main__":
    unittest.main()
