# 1306 Доможиров Денис
import unittest
from COM_NN_D import COM_NN_D
class SUB_NN_N:
    def __init__(self):
        pass

    def SUB_NN_N(self, D1, D2):
        # если числа равны, то выводим 0
        if COM_NN_D().COM_NN_D(D1, D2) == 0:   return "0"
        # описание алгоритма для разных чисел
        D1 = [int(x) for x in D1][::-1]
        D2 = [int(x) for x in D2][::-1]
        D3 = []
        tmp = 0
        i = 0
        # создаем цикл для вычитания чисел
        for i in range(min(len(D1), len(D2))):
            # занимаем 1 у последующего разряда
            if (D1[i] - D2[i] - tmp < 0):
                D3.append(D1[i] - D2[i] - tmp + 10)
                tmp = 1
            else:
                D3.append(D1[i] - D2[i] - tmp)
                tmp = 0
        # сравнение длины числа и счетчика
        if (len(D1) - 1 > i):
            # замена 10 на 9 в числе
            while D1[i + 1] == 0 and tmp:
                D3.append(9)
                i += 1
            if (D1[i + 1] - tmp >= 0):
                D3.append(D1[i + 1] - tmp)
                i += 1
            # сравнение длины числа и счетчика
            if (i < len(D1) - 1):
                D3 += D1[i + 1:len(D1)]
        D3 = D3[::-1]
        while D3[0] == 0:
            D3 = D3[1:]
        return "".join(str(x) for x in D3)

class TestSUB_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = SUB_NN_N()

    def test_add(self):
        self.assertEqual(self.module.SUB_NN_N("1","1"), "0")
        self.assertEqual(self.module.SUB_NN_N("123551", "45622"), "77929")
        self.assertEqual(self.module.SUB_NN_N("24", "10"),"14")
        self.assertEqual(self.module.SUB_NN_N("88005553535", "10000000000"), "78005553535")
        self.assertEqual(self.module.SUB_NN_N("23424","124"),"23300")
# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()














