#Ruslan Isaev 1306
import unittest
from str_natural_to_arr import Str_D_To_Arr

class NZER_N_B:
    def __init__(self):
        pass

    def NZER_N_B(self, A): #Смотрим 1 элемента строки и сравниваем с 0
        A = Str_D_To_Arr(A)
        if (len(A) != 1): #Если число больше 1 разряда то оно не 0. выводим да      
            return True 
        elif (A[0] == 0): #1 разряд равен 0 значит нет
            return False
        else:
            return True #Если число 1 разряда и не 0 то выводим да


class TestNZER_N_B(unittest.TestCase):
    def setUp(self):
        self.module = NZER_N_B()

    def test_add(self):
        self.assertEqual(self.module.NZER_N_B("0"), False)

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
