# 1306 Тряпша Лиза
import unittest
from str_natural_to_arr import Str_D_To_Arr

class ADD_1N_N:
    def __init__(self):
        pass

    def ADD_1N_N(self, d):
        f = Str_D_To_Arr(d)
        b = ['0'] + f  #запасной символ
        n = len(b)
        k = len(b) - 1
        # все символы '9' заменяем на нули
        while b[k] == 9:
            b[k] = 0
            k -= 1
        b[k] = int(b[k]) + 1
        # удаляем при наличии лишний символ
        if b[0] == '0':
            b[0] = ''

        g = "".join(map(str, b))
        return g

class TestADD_1N_N(unittest.TestCase):
    def setUp(self):
        self.module = ADD_1N_N()

    def test_add(self):
        self.assertEqual(self.module.ADD_1N_N('99999495995969'), '99999495995970')

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
