# 1306 Павел Смуров
import unittest
from str_rational_to_arr import Str_Q_To_Arr
from GCF_NN_N import GCF_NN_N
from ABS_Z_N import ABS_Z_N
from DIV_ZZ_Z import DIV_ZZ_Z
from TRANS_Q_Z import TRANS_Q_Z


class RED_Q_Q:
    def __init__(self):
        pass

    def RED_Q_Q(self, Q):
        F = Str_Q_To_Arr(Q)
        # Разделяем дробь на числитель и знаменатель
        if ABS_Z_N().ABS_Z_N(F[0]) != "0" and ABS_Z_N().ABS_Z_N(F[1]) != "0":  # если нет нуля в числителе или знаменателе
            nod = GCF_NN_N().GCF_NN_N(ABS_Z_N().ABS_Z_N(F[0]), ABS_Z_N().ABS_Z_N(F[1]))
            # Находим НОД числителя и знаменателя
            F[0] = DIV_ZZ_Z().DIV_ZZ_Z(F[0], nod)
            F[1] = DIV_ZZ_Z().DIV_ZZ_Z(F[1], nod)
            # Делим числитель и знаменатель на их НОД и получаем сокращённую дробь
            res = str(F[0]) + "/" + str(F[1][1:])
            # Собираем дробь обратно в строку
            res = TRANS_Q_Z().TRANS_Q_Z(res)
            return res
        else:
            return TRANS_Q_Z().TRANS_Q_Z(Q)


class TestRED_Q_Q(unittest.TestCase):
    def setUp(self):
        self.module = RED_Q_Q()

    def test_add(self):
        self.assertEqual(self.module.RED_Q_Q("+8/12"), "+2/3")
        self.assertEqual(self.module.RED_Q_Q("-360/2940"), "-6/49")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
