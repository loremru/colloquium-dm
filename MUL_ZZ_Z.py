#1306 Кравченко Софья
import unittest
from POZ_Z_D import POZ_Z_D
from ABS_Z_N import ABS_Z_N
from MUL_NN_N import MUL_NN_N
from MUL_ZM_Z import MUL_ZM_Z


class MUL_ZZ_Z:
    def __init__(self):
        pass
    
    def MUL_ZZ_Z(self, Z1, Z2):
        if Z1 == "0" or Z2 == "0":
            Z_res = "0"
        else:
            #возьмем числа по модулю, перемножим как натуральные
            ABS_Z1 = ABS_Z_N().ABS_Z_N(Z1)
            ABS_Z2 = ABS_Z_N().ABS_Z_N(Z2)

            #запишем с плюсом, если нужно изменим знак позже
            Z_res = "+" + MUL_NN_N().MUL_NN_N(ABS_Z1, ABS_Z2)

            # если знаки разные, умножаем на -1
            if POZ_Z_D().POZ_Z_D(Z1) != POZ_Z_D().POZ_Z_D(Z2):
                Z_res = MUL_ZM_Z().MUL_ZM_Z(Z_res)
        return Z_res




class Test_MUL_ZZ_Z(unittest.TestCase):
    
    def setUp(self):
        self.module = MUL_ZZ_Z()
    
    def test_add(self):
        self.assertEqual(self.module.MUL_ZZ_Z("0", "0"),
                         "0")
        self.assertEqual(self.module.MUL_ZZ_Z("-125", "+4"),
                         "-500")
        self.assertEqual(self.module.MUL_ZZ_Z("-1000000001", "-9"),
                         "+9000000009")
        self.assertEqual(self.module.MUL_ZZ_Z("-5", "0"), "0")
        self.assertEqual(self.module.MUL_ZZ_Z("125", "791"), "+98875")
        self.assertEqual(self.module.MUL_ZZ_Z("7614", "+9888"), "+75287232")
        self.assertEqual(self.module.MUL_ZZ_Z("+21345667987", "+456985"),
                         "+9754650085039195")


if __name__ == "__main__":
    unittest.main()