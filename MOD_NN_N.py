# 1306 Вилков Дмитрий
import unittest
from DIV_NN_N import DIV_NN_N
from SUB_NDN_N import SUB_NDN_N
from COM_NN_D import COM_NN_D
#MOD_NN_N

class MOD_NN_N:
    def __init__(self):
        pass
    def MOD_NN_N(self, Z1, Z2):
        if COM_NN_D().COM_NN_D(Z1, Z2)==1: #если второе больше перовго меняем местами
            Z1, Z2 = Z2, Z1
        tmp=DIV_NN_N().DIV_NN_N(Z1,Z2) #деление числа без остатка
        return SUB_NDN_N().SUB_NDN_N(Z1,tmp,Z2) #возвращаем разность большего числа и произведения меньшего на результат делания
        
class TestMOD_NN_N(unittest.TestCase):
    def setUp(self):
        self.module = MOD_NN_N()

    def test_add(self):
        self.assertEqual(self.module.MOD_NN_N("31", "2"), "1")
        self.assertEqual(self.module.MOD_NN_N("30", "2"), "0")
        self.assertEqual(self.module.MOD_NN_N("98", "3"), "2")
        self.assertEqual(self.module.MOD_NN_N("76329675", "76329675"), "0")
        self.assertEqual(self.module.MOD_NN_N("3", "31"), "1")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
