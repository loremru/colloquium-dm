#Бельский Игорь 1306
import unittest
from str_natural_to_arr import Str_D_To_Arr
class COM_NN_D:
    def __init__(self):
        pass
    def COM_NN_D(self, D1, D2):
        #преобразовываем строки в списки
        A1 = Str_D_To_Arr(D1)
        A2 = Str_D_To_Arr(D2)
        #находим длины списков
        n1 = len(A1)
        n2 = len(A2)
        #сравниваем по длине
        if n1 > n2:
            return 2
        elif n1 < n2:
            return 1
        else:
            #сравниваем по разрядам
            for i in range(n1):
                if A1[i] > A2[i]:
                    return 2
                elif A1[i] < A2[i]:
                    return 1
        return 0

class TestCOM_NN_D(unittest.TestCase):
    def setUp(self):
        self.module = COM_NN_D()

    def test_add(self):
        self.assertEqual(self.module.COM_NN_D("1234", "1235"), 1)

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
