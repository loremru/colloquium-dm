# 1306 Смуров Павел
import unittest
from ABS_Z_N import ABS_Z_N
from str_rational_to_arr import Str_Q_To_Arr

class TRANS_Q_Z:
    def __init__(self):
        pass

    def TRANS_Q_Z(self, Q):
        A = Str_Q_To_Arr(Q)
        if A[1] == "1" or ABS_Z_N().ABS_Z_N(A[0]) == "0":
            return str(A[0])
        return Q


class TestTRANS_Q_Z(unittest.TestCase):
    def setUp(self):
        self.module = TRANS_Q_Z()

    def test_add(self):
        self.assertEqual(self.module.TRANS_Q_Z("1582/1"), ("1582"))  # Вводить числа через фукнцию Льва(Ауф)
        self.assertEqual(self.module.TRANS_Q_Z("282/1"), ("282"))
        self.assertEqual(self.module.TRANS_Q_Z("1582/123"), ("1582/123"))
        self.assertEqual(self.module.TRANS_Q_Z("+0/123"), ("+0"))


# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
