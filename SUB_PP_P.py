#Исаев Руслан 1306

import unittest

from str_poly_to_arr import str_p_to_arr

from arr_poly_to_str import arr_p_to_str

from SUB_QQ_Q import SUB_QQ_Q

from TRANS_Z_Q import TRANS_Z_Q

from RED_Q_Q import RED_Q_Q

class SUB_PP_P:
    def __init__(self):
        pass
    
    def SUB_PP_P(self,C1,C2):
        a=str_p_to_arr(C1) #переводим многочлены из строк в массивы коэффициентов
        b=str_p_to_arr(C2)
        a=a[::-1] #переворачиваем массивы для удобства вычисления
        b=b[::-1]
        for i in range(len(b)):
            if not "/" in a[i]: #если  коэффициент НЕ дробный, то делаем его дробью со знам. = 1
                a[i]=TRANS_Z_Q().TRANS_Z_Q(a[i])
            if not "/" in b[i]: #если  коэффициент НЕ дробный, то делаем его дробью со знам. = 1
                b[i]=TRANS_Z_Q().TRANS_Z_Q(b[i])
            a[i]=SUB_QQ_Q().SUB_QQ_Q(a[i],b[i]) #вычитаем коэффициенты
            a[i]=RED_Q_Q().RED_Q_Q(a[i]) #сокращаем дробь
        return arr_p_to_str(a[::-1])

class TestSUB_PP_P(unittest.TestCase):
    def setUp(self):
        self.module = SUB_PP_P()
    def test_add(self):
        self.assertEqual(self.module.SUB_PP_P("+5,+1,-3,+2,+2","+2,+2,+2"),"+5, +1, -5, 0, 0")
        self.assertEqual(self.module.SUB_PP_P("+5,+1,+3,+2,+2","+2,+2,+2"),"+5, +1, +1, 0, 0")

if __name__ == "__main__":
    unittest.main()
