import unittest
from str_poly_to_arr import str_p_to_arr
from arr_poly_to_str import arr_p_to_str
from RED_Q_Q import RED_Q_Q
from MUL_QQ_Q import MUL_QQ_Q
 
class MUL_PQ_P:
    def __init__(self):
        pass
 
    def MUL_PQ_P(self, C, Q):
        a = str_p_to_arr(C)
        for i in range(len(a)):
            if not "/" in a[i]:  # если  коэффициент НЕ дробный, то делаем его дробью со знам. = 1
                a[i] = RED_Q_Q().RED_Q_Q(a[i])
            a[i] = MUL_QQ_Q().MUL_QQ_Q(a[i], Q)  # перемножаем коэффициенты
            a[i] = RED_Q_Q().RED_Q_Q(a[i])  # делаем число целым
        return arr_p_to_str(a)
 
 
class TestMUL_PQ_P(unittest.TestCase):
    def setUp(self):
        self.module = MUL_PQ_P()
 
    def test_add(self):
        self.assertEqual(self.module.MUL_PQ_P("+5,+1,+0,-2,+2", "5/8"), "+25/8, +5/8, +0, -5/4, +5/4")
 
 
 
# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
