#1306 Васильев Сергей

import unittest

class TRANS_Z_N:

    def __init__(self):
        pass

    def trans_z_n (self, a):
        if a[0] == '-':
            print('ERROR! -> function works only with positive numbers')
        else:
            if a[0] == '+':
                res = a[1:]
            else:
                res = a

            return res

class Testtrans_z_n(unittest.TestCase):
    def setUp(self):
        self.module = TRANS_Z_N()

    def test_add(self):
        self.assertEqual(self.module.trans_z_n('+234'), '234')

if __name__ == '__main__':
    unittest.main()
