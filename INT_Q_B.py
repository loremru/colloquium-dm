#1306 Васильев Сергей

import unittest
from MOD_ZZ_Z import MOD_ZZ_Z

class INT_Q_B:

    def __init__(self):
        pass

    #функция проверки корректности записи числа
    def checkinput(self,a):
        arr = '0123456789'
        summ = 0
        res = 0

        for i in arr:
            summ+=a.count(i)

        if summ<len(a):
            res = 1

        if a[0] == '0' and len(a)>1:
            res = 1
        
        return res

    #сравнение чисел
    def compare(self,a,b):
        res = 0
        if len(a) > len(b):
            res = 1
        elif len(a) < len(b):
            res = 2
        else:
            for i in range (len(a)):
                if a[i] > b[i]:
                    res = 1
                    break
                if a[i] < b[i]:
                    res = 2
                    break
        return res
    
    def INT_Q_B (self,z):
        S = 0
        res = 'ERR'

        #разделение на 2 числа
        n = z[z.index('/')+1:]
        z = z[:z.index('/')]

        #проверка на ввод по условиям
        if (n[0] == '0') or ((n[0] == '+') and (n[1] == '0')) or ((n[0] == '-') and (n[1] == '0')):
            print('ERROR! -> incorrect entry of a rational number -> division by zero')
            S = 1
        elif (n[0] == '-') or ('.' in n) or (',' in n):
            print('ERROR! -> incorrect entry of a rational number -> the denominator must be a natural number')
            S = 1
        elif ('.' in z) or (',' in z):
            print('ERROR! -> incorrect entry of a rational number -> the numerator must be an integer number')
            S = 1

        if (z[0] == '+') or (z[0] == '-'):
            z = z[1:]
        if n[0] == '+':
            n = n[1:]

        #проверка на корректную запись введённого числа
        if INT_Q_B().checkinput(z) == 1:
            print('ERROR! -> incorrect entry of a rational number -> the numerator must be an integer number')
            S = 1
        if INT_Q_B().checkinput(n) == 1:
            print('ERROR! -> incorrect entry of a rational number -> the denominator must be a natural number')
            S = 1

        #проверка на делимость
        if S!=1:
            if n == '1':
                res = 'YES'
            elif INT_Q_B().compare(z,n) == 2:
                res = 'NO'
            elif INT_Q_B().compare(z,n) == 0:
                res = 'YES'
            elif MOD_ZZ_Z().MOD_ZZ_Z(z,n) == '0' or MOD_ZZ_Z().MOD_ZZ_Z(z,n) == '-0' or MOD_ZZ_Z().MOD_ZZ_Z(z,n) == '+0':
                res = 'YES'
            else:
                res = 'NO'

        return res
        
class Testint_q_b(unittest.TestCase):
    def setUp(self):
        self.module = INT_Q_B()

    def test_add(self):
        self.assertEqual(self.module.INT_Q_B('444/111'),'YES')

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == '__main__':
    unittest.main()
