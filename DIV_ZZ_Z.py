# Богданов Лев 1306
from COM_NN_D import COM_NN_D

from POZ_Z_D import POZ_Z_D

from ABS_Z_N import ABS_Z_N

from DIV_NN_N import DIV_NN_N

from MUL_ZZ_Z import MUL_ZZ_Z

from ADD_1N_N import ADD_1N_N

import unittest

class DIV_ZZ_Z:
    def __init__(self):
        pass

    def DIV_ZZ_Z(self,D1,D2):
        if "+" in D1:
            D1=D1[1:]
        if "+" in D2:
            D2=D2[1:]
        if COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(D1), ABS_Z_N().ABS_Z_N(D2)) == 1:  # в случае, если D2 больше D1 ПО МОДУЛЮ, рассматриваем случаи:
            if POZ_Z_D().POZ_Z_D(D1)==1 and POZ_Z_D().POZ_Z_D(D2)==1: #если оба числа отрицательные, то частное будет равно единице
                return "+1"
            elif POZ_Z_D().POZ_Z_D(D1)==1 and POZ_Z_D().POZ_Z_D(D2)==2: #если только числитель отрицательный, то частное = -1
                return "-1"
            elif POZ_Z_D().POZ_Z_D(D1)==2 and POZ_Z_D().POZ_Z_D(D2)==1: #если только знаменатель отрицательный, то частное = 1
                return "+1"
        if COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(D1), ABS_Z_N().ABS_Z_N(D2)) == 0:  # в случае, если D1 = D2, выводим частное = 1, следим за знаком
            if (POZ_Z_D().POZ_Z_D(D1)==2 and POZ_Z_D().POZ_Z_D(D2)==2) or (POZ_Z_D().POZ_Z_D(D1)==1 and POZ_Z_D().POZ_Z_D(D2)==1):
                return "+1"
            else:
                return "-1"
        if POZ_Z_D().POZ_Z_D(D1)==2 and POZ_Z_D().POZ_Z_D(D2)==2: # если оба положительные, то просто используем DIV_NN_N
            return ("+" + DIV_NN_N().DIV_NN_N(D1,D2))
        elif POZ_Z_D().POZ_Z_D(D1)==1 and POZ_Z_D().POZ_Z_D(D2)==1: # если оба числа отрицательные, то ответ будет положительным, используем DIV_NN_N на модуле чисел
            return ("+" + ADD_1N_N().ADD_1N_N(DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), ABS_Z_N().ABS_Z_N(D2))))
        else:
            if POZ_Z_D().POZ_Z_D(D1)==1: #Если первое число D1 было отрицательным, то модуль в DIV_NN_N берётся модуль от D1
                if COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(MUL_ZZ_Z().MUL_ZZ_Z(DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), D2), D2)), ABS_Z_N().ABS_Z_N(D1)) == 2: #Доп. проверка для отрицательых
                    return ("-" + DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), D2))
                elif COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(MUL_ZZ_Z().MUL_ZZ_Z(DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), D2), D2)), ABS_Z_N().ABS_Z_N(D1)) == 0: 
                    return ("-" + DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), D2))
                else:
                    return ("-" + ADD_1N_N().ADD_1N_N(DIV_NN_N().DIV_NN_N(ABS_Z_N().ABS_Z_N(D1), D2)))
            else: #Если второе число D2 было отрицательным, то модуль в DIV_NN_N берётся модуль от D2
                if COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(MUL_ZZ_Z().MUL_ZZ_Z(DIV_NN_N().DIV_NN_N(D1, ABS_Z_N().ABS_Z_N(D2)), ABS_Z_N().ABS_Z_N(D2))), ABS_Z_N().ABS_Z_N(D1)) == 2: #Доп. проверка для отрицательных
                    return ("-" + DIV_NN_N().DIV_NN_N(D1, ABS_Z_N().ABS_Z_N(D2)))
                elif COM_NN_D().COM_NN_D(ABS_Z_N().ABS_Z_N(MUL_ZZ_Z().MUL_ZZ_Z(DIV_NN_N().DIV_NN_N(D1, ABS_Z_N().ABS_Z_N(D2)), ABS_Z_N().ABS_Z_N(D2))), ABS_Z_N().ABS_Z_N(D1)) == 0: 
                    return ("-" + DIV_NN_N().DIV_NN_N(D1, ABS_Z_N().ABS_Z_N(D2)))
                else:
                    return ("-" + ADD_1N_N().ADD_1N_N(DIV_NN_N().DIV_NN_N(D1, ABS_Z_N().ABS_Z_N(D2))))

class TestDIV_ZZ_Z(unittest.TestCase):
    def setUp(self):
        self.module = DIV_ZZ_Z()

    def test_add(self):
        self.assertEqual(self.module.DIV_ZZ_Z("144","-144"), "-1")
        self.assertEqual(self.module.DIV_ZZ_Z("-12345","+123"), "-101")
        self.assertEqual(self.module.DIV_ZZ_Z("+1337","+228"), "+5")
        self.assertEqual(self.module.DIV_ZZ_Z("-1234","-19876"), "+1")
        self.assertEqual(self.module.DIV_ZZ_Z("+1234","-19876"), "+1")
        self.assertEqual(self.module.DIV_ZZ_Z("-19876","-1234"), "+17")
        self.assertEqual(self.module.DIV_ZZ_Z("+19876","+1234"), "+16")
        self.assertEqual(self.module.DIV_ZZ_Z("-19876","+1234"), "-17")
        self.assertEqual(self.module.DIV_ZZ_Z("+19876","-1234"), "-17")
        self.assertEqual(self.module.DIV_ZZ_Z("-4234","+23"), "-185")
        self.assertEqual(self.module.DIV_ZZ_Z("+21312","+1323"), "+16")
        self.assertEqual(self.module.DIV_ZZ_Z("-360","+60"), "-6")
        self.assertEqual(self.module.DIV_ZZ_Z("-125","-17"), "+8")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
