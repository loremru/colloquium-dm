# 1306 Kolosov Kostyan
import unittest
from MUL_ZZ_Z import MUL_ZZ_Z
from RED_Q_Q import RED_Q_Q
from str_rational_to_arr import Str_Q_To_Arr


class MUL_QQ_Q:
    def __init__(self):
        pass

    def MUL_QQ_Q(self, Q01, Q02):
        A = Str_Q_To_Arr(Q01)  # разделяем дроби на знаменатели и числители
        B = Str_Q_To_Arr(Q02)  #

        A1 = A[0]  # знаменатель первой дроби
        B1 = A[1]  # числитель первой дроби
        A2 = B[0]  # знаменатель второй дроби
        B2 = B[1]  # числитель второй дроби

        A = MUL_ZZ_Z().MUL_ZZ_Z(A1, A2)  # умножение знаменателей и числителей
        B = MUL_ZZ_Z().MUL_ZZ_Z(B1, B2)

        Q = str(A) + "/" + str(B)  # собираем дробь в строку
        return RED_Q_Q().RED_Q_Q(Q)  # вывод дроби с сокращением


class TestMUL_QQ_Q(unittest.TestCase):
    def setUp(self):
        self.module = MUL_QQ_Q()

    def test_add(self):
        self.assertEqual(self.module.MUL_QQ_Q("2/7", "5/8"), "+5/28")
        self.assertEqual(self.module.MUL_QQ_Q("4/7", "9/8"), "+9/14")
        self.assertEqual(self.module.MUL_QQ_Q("3/2", "2/1"), "+3")
        self.assertEqual(self.module.MUL_QQ_Q("79/156", "156/79"), "+1")
        self.assertEqual(self.module.MUL_QQ_Q("-79/156", "156/79"), "-1")

# Если запускаем этот файл, то запускается тестирование модуля
if __name__ == "__main__":
    unittest.main()
    # print(MUL_QQ_Q().MUL_QQ_Q("3/2", "2/1"))

   